#!/usr/bin/env sh

export ENV=$HOME/.ashrc
export BROWSER=w3m
export PAGER="less -IM"
export EDITOR=vim
export SUDO_EDITOR=vim
export VISUAL=vim
export MANPAGER="less -IM"
export LANG=en_US.UTF-8
export LC_TYPE=en_US.UTF-8
#PATH="$HOME/bin:$PATH"

#if [ -d "$HOME/bin" ]; then PATH="$HOME/bin:$PATH"; fi

if [ -z "$DISPLAY" ]; then exec startx; fi
#case $- in *i*) [-z "$TMUX" ] && exec tmux ;; esac
