#!/bin/sh

apk add vim git -yy ;
vim /etc/apk/repositories ; #change version 3.xx to latest-stable
apk update; apk upgrade -yy ;
yes | apk add git man-pages mandoc mandoc-apropos docs vim shellcheck tmux imagemagick ncdu htop newsboat mutt isync w3m lynx curl wget mc nnn cmus mpv ffmpeg sox mediainfo vlc gstreamer gstreamer-tools mpg123 tvheadend cups nyancat cmatrix neofetch openssh openssh-server transmission transmission-cli fzf python abook calcurse libcaca weechat irssi irssi-xmpp tor proxychains-ng pwgen mosh rsync zip unzip minicom minidlna ncurses udisks2 terminus-font dbus scrcpy android-tools yank tty-copy croc zellij -yy ;
#setup-xorg-base ;
#apk add bspwm sxhkd sakura rofi polybar vimb xf86-video-intel xf86-input-intel xf86-video-fbdev xf86-video-vesa xf86-input-libinput xf86-input-evdev xf86-input-mouse xf86-input-keyboard xf86-input-synaptics dbus-x11 gvfs gvfs-nfs gvfs-mtp gvfs-smb ntfs-3g gvfs-fuse -y 
#apk add linux-headers libxinerama-dev libxft-dev libx11-dev ncurses git gcc g++ make dbus-x11 -y ; git clone git://git.suckless.org/dwm
yes | apk add fuse-openrc ; rc-service start fuse ; rc-update add fuse ;;
yes | apk add dbus ; rc-service start dbus ; rc-update add dbus ; dbus-uuidgen > /var/lib/dbus/machine-id
setup-devd udev ;
reboot

#ENV=$HOME/.ashrc; export ENV; . $ENV ##how to source ashrc in .profile

#Due to Alpine by default waiting for time sync and trying to obtain a dhcp lease where it also waits and tries a few times booting can be slow compared to other distro's. Luckily this can easily be fixed by editing some files. First we make the dhcp client send only one request at boot and then fork to the background. This is done by editing /etc/networking/interfaces and adding a line under each iface declaration saying: udhcpc_opts -b -t 1 -T 1. It will try once and wait for 1 second before forking. To make it not wait for a time sync for too long we can edit /etc/conf.d/chronyd and set the FAST_STARTUP option to yes. Lastly try enabling rc_parallel in /etc/rc.conf though some services might not like it, I've rarely had issues with it.